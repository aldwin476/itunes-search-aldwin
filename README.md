# README #

Persistence:
*Used Room database to save data inside the app. I used Room because it is a database that is already provided by android which means that I can expect full support when it comes to implementation. Other thing is that it is really easy to use and implement
*Room database is used inside the application on saving items to the favorites tab. The data inside the favorites tab are available even on offline mode. I also included viewing of offline data on the details page.
*KotPref was used inside the application on saving basic data like the Last Visited Page which were used on switching fragments inside the application.

Architecture:
* MVVM Architecture was used on the project because I find it more maintanable and when it comes to bigger projects, it will be easier to implement and remove features when needed.
* Reactive programming was also used on the implementation of MVVM architecture
* Navigation Arcitecture was also used on the project for the switching of pages inside the app
* The application is a single activity application which means that it only contains one activity and the other pages are all fragments. A single activity application is much faster than using different Activities for all the pages. It also better user experience since the user can navigate to different pages faster than having different transitions on switching between activites
* Koin dependency injection was also used on the architecture