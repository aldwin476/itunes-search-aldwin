package com.appetiser.aldwin.domain.entity

import androidx.room.*
import java.io.Serializable

@Entity(tableName = "results_table")
data class ResultEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "trackName") val trackName: String,
    @ColumnInfo(name = "trackId") val trackId: Int,
    @ColumnInfo(name = "price") val price: Double,
    @ColumnInfo(name = "genre") val genre: String,
    @ColumnInfo(name = "artworkSrc") val artworkSrc: String,
    @ColumnInfo(name = "artistName") val artistName: String,
    @ColumnInfo(name = "releaseDate") val releaseDate: String,
    @ColumnInfo(name = "longDescription") val longDescription: String?,
    @ColumnInfo(name = "shortDescription") val shortDescription: String?
) : Serializable


