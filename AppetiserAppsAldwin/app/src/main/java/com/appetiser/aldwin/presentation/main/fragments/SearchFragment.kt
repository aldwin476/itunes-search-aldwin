package com.appetiser.aldwin.presentation.main.fragments

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.aldwin.R
import com.appetiser.aldwin.data.local.Preferences.lastVisitedDate
import com.appetiser.aldwin.data.local.Preferences.lastVisitedPage
import com.appetiser.aldwin.data.local.Preferences.searchText
import com.appetiser.aldwin.data.local.Preferences.navSource
import com.appetiser.aldwin.data.remote.model.ResultsModel
import com.appetiser.aldwin.presentation.main.viewmodels.ITunesEvent
import com.appetiser.aldwin.presentation.main.viewmodels.MainViewModel
import com.appetiser.aldwin.util.ITunesItems
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.include_navigation_menu.*
import org.koin.androidx.viewmodel.ext.viewModel

class SearchFragment : Fragment() {
    private val viewModel: MainViewModel by viewModel()
    private var groupAdapter = GroupAdapter<GroupieViewHolder>()
    var iTunesItems: MutableList<ITunesItems> = mutableListOf()
    var resultItems: List<ResultsModel> = mutableListOf()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.event.observe(this, Observer { event ->
            when (event) {
                is ITunesEvent.OnSearchResults -> onSearchResults(event.results)
            }
        })
        tv_last_visited_date.text = lastVisitedDate
        lastVisitedPage = "search"
        navSource = "search"
        nav_back_btn.visibility = View.GONE
        nav_title.text = "Search"
        rv_itunes_items.apply {
            //Groupie Adapter to be used on the recycler
            groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
                setOnItemClickListener { item, view ->
                    lastVisitedPage = "search"
                    var bundle = bundleOf("result" to resultItems[getAdapterPosition(item)])
                    view.findNavController().navigate(R.id.main_to_details, bundle)
                }
            }
            layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = groupAdapter
        }
        et_search.setText(searchText)
        viewModel.itunesSearch(searchText)
        et_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            //Contains action when search text was changed which will call the api to search new items
            override fun afterTextChanged(s: Editable) {
                searchText = s.toString()
                viewModel.itunesSearch(s.toString())

            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_search, container, false)


    //Contains actions when successfully loaded data from api
    private fun onSearchResults(results: List<ResultsModel>) {
        resultItems = results
        iTunesItems = mutableListOf()
        results.forEach { item ->
            iTunesItems.add(
                ITunesItems(
                    item.artworkUrl100,
                    item.trackName,
                    item.primaryGenreName,
                    item.trackPrice,
                    context!!
                )
            )
        }
        groupAdapter.clear()
        groupAdapter.addAll(iTunesItems)


    }
}