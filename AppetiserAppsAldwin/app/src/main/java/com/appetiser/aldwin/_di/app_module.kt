package com.appetiser.aldwin._di


/** App Components for Dependency Injection **/

val appModules = listOf(viewModelModule, dataDomainModule)