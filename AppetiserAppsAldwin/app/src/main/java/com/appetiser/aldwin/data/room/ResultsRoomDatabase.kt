package com.appetiser.aldwin.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.appetiser.aldwin.domain.entity.ResultEntity

@Database(entities = [ResultEntity::class], version = 4, exportSchema = false)
abstract class ResultsRoomDatabase : RoomDatabase() {
    abstract fun resultsDao(): ResultsDao

    companion object {

        /**
         * This is just for singleton pattern
         */
        private var INSTANCE: ResultsRoomDatabase? = null

        fun getDatabase(context: Context): ResultsRoomDatabase {
            if (INSTANCE == null) {
                synchronized(ResultsRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            ResultsRoomDatabase::class.java, "results_room_database"
                        )
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}