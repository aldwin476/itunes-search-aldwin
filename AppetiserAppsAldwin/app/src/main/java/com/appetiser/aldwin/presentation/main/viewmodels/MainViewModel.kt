package com.appetiser.aldwin.presentation.main.viewmodels

import com.appetiser.aldwin.domain.ITunesRepository
import com.appetiser.aldwin.data.remote.model.ResultsModel
import com.appetiser.utilities.coroutines.RxViewModel
import com.appetiser.utilities.coroutines.SchedulerProvider
import com.appetiser.utilities.coroutines.SingleLiveEvent
import com.appetiser.utilities.coroutines.with
import io.reactivex.rxkotlin.subscribeBy

class MainViewModel(
    private val iTunesRepository: ITunesRepository,
    private val schedulerProvider: SchedulerProvider
) : RxViewModel() {


    private val _event = SingleLiveEvent<ITunesEvent>()
    val event: SingleLiveEvent<ITunesEvent> get() = _event

    /**
     * Search items from API
     *
     * @param term Any [String] that will be used to search items on the api
     */
    fun itunesSearch(term: String?) {
        launch {
            iTunesRepository.search(term).with(schedulerProvider)
                .subscribeBy(
                    onComplete = {
                        _event.value =
                            ITunesEvent.OnSearchResults(
                                iTunesRepository.results
                            )
                    },
                    onError = {
                    })
        }
    }
}

//Event for handling actions on the viewmodel
sealed class ITunesEvent {
    data class OnSearchResults(val results: List<ResultsModel>) : ITunesEvent()
}