package com.appetiser.aldwin.data.remote

import android.net.Uri
import com.appetiser.aldwin.BuildConfig
import com.appetiser.aldwin.data.remote.model.ITunesModel

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*


interface ITunesService {

    companion object {
        private const val URI_SCHEME = "https"

        private const val URI_AUTHORITY_DEBUG = "itunes.apple.com"
        private const val URI_AUTHORITY_STAGING = "itunes.apple.com"
        private const val URI_AUTHORITY_RELEASE = "itunes.apple.com"

        val baseUrl
            get() = Uri.Builder().apply {
                scheme(URI_SCHEME)
                authority(
                    when (BuildConfig.BUILD_TYPE) {
                        "debug" -> URI_AUTHORITY_DEBUG
                        "staging" -> URI_AUTHORITY_STAGING
                        "release" -> URI_AUTHORITY_RELEASE
                        else -> URI_AUTHORITY_DEBUG
                    }
                )
                appendPath("")
            }.build().toString()
    }


    @GET("search")
    fun getItems(
        @Query("term") term: String? = "star",
        @Query("country") searchcountry: String? = "au",
        @Query("media") media: String? = "movie"
    )
            : Single<Response<ITunesModel>>

}
