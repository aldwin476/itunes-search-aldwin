package com.appetiser.aldwin.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.aldwin.domain.entity.ResultEntity

@Dao
interface ResultsDao {
    @Query("SELECT * from results_table ORDER BY trackName ASC")
    fun getAllSavedData(): List<ResultEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(word: ResultEntity?)

    @Query("DELETE FROM results_table")
    fun deleteAll()

    @Query("SELECT * from results_table WHERE trackId = :id")
    fun getByTrackId(id: Int): ResultEntity

    @Query("DELETE  from results_table WHERE trackId = :id")
    fun deleteById(id: Int)
}