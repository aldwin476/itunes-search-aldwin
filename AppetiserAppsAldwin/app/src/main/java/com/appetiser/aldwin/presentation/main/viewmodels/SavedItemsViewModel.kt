package com.appetiser.aldwin.presentation.main.viewmodels

import com.appetiser.aldwin.data.room.ITunesRoomRepository
import com.appetiser.aldwin.domain.entity.ResultEntity
import com.appetiser.utilities.coroutines.RxViewModel
import com.appetiser.utilities.coroutines.SchedulerProvider
import com.appetiser.utilities.coroutines.SingleLiveEvent


class SavedItemsViewModel(
    private val iTunesRoomRepository: ITunesRoomRepository,
    private val schedulerProvider: SchedulerProvider
) : RxViewModel() {
    private val _event = SingleLiveEvent<SavedItemEvent>()
    val event: SingleLiveEvent<SavedItemEvent> get() = _event


    // Get Items from database
    fun getSavedData() {
        iTunesRoomRepository.getAllSavedData()
        _event.value =
            SavedItemEvent.OnGetSavedData(
                iTunesRoomRepository.getAllSavedData()
            )

    }
}

//Event for handling actions on the viewmodel
sealed class SavedItemEvent {
    data class OnGetSavedData(val results: List<ResultEntity>) : SavedItemEvent()
}