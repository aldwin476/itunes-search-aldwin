package com.appetiser.aldwin._di

import com.appetiser.aldwin.domain.ITunesRepository
import com.appetiser.aldwin.domain.ITunesRepositoryImpl
import com.appetiser.aldwin.data.remote.ITunesService
import com.appetiser.aldwin.data.room.ITunesRoomRepository
import com.appetiser.aldwin.data.room.ITunesRoomRepositoryImpl
import com.appetiser.utilities.coroutines.AppSchedulerProvider
import com.appetiser.utilities.coroutines.SchedulerProvider
import com.appetiser.utilities.helpers.WebService

import org.koin.dsl.module

val dataDomainModule = module {
    single<ITunesRepository>(createdAtStart = true) { ITunesRepositoryImpl(get()) }
    single<ITunesRoomRepository>(createdAtStart = true) { ITunesRoomRepositoryImpl(get()) }
    single<SchedulerProvider>(createdAtStart = true) { AppSchedulerProvider() }
    single {
        WebService.create<ITunesService>(
            ITunesService.baseUrl
        )
    }


}

