package com.appetiser.aldwin.data.local

import com.chibatching.kotpref.KotprefModel

object Preferences : KotprefModel() {
    var searchText by stringPref()
    var navSource by stringPref()
    var lastVisitedDate by stringPref()
    var lastVisitedPage by stringPref()
    var detailsId by intPref()
}