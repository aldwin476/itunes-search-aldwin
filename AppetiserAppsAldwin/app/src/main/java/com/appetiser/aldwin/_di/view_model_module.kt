package com.appetiser.aldwin._di


import com.appetiser.aldwin.presentation.main.viewmodels.DetailsViewModel
import com.appetiser.aldwin.presentation.main.viewmodels.MainViewModel
import com.appetiser.aldwin.presentation.main.viewmodels.SavedItemsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        MainViewModel(
            get(),
            get()
        )
    }
    viewModel {
        DetailsViewModel(
            get()
        )
    }
    viewModel {
        SavedItemsViewModel(
            get(),
            get()
        )
    }


}