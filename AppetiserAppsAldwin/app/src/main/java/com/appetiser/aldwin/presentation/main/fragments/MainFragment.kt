package com.appetiser.aldwin.presentation.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.appetiser.aldwin.R
import com.appetiser.aldwin.data.local.Preferences.navSource
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {
    private var currentSelected = R.id.navigation_search

    //Listener for bottom navigation
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_search -> {
                    val fragment =
                        SearchFragment()
                    currentSelected = R.id.navigation_search
                    activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_saved -> {
                    val fragment =
                        SavedDataFragment()
                    currentSelected = R.id.navigation_saved
                    activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }

            }
            false
        }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigationView.menu.findItem(currentSelected).isChecked = true

        //Checker on which tab should display
        when (navSource) {
            "favorites" -> bottomNavigationView.selectedItemId = R.id.navigation_saved
            else -> bottomNavigationView.selectedItemId = R.id.navigation_search
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_main, container, false)


}