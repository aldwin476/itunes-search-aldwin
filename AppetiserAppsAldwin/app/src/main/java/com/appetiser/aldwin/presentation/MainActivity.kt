package com.appetiser.aldwin.presentation
import android.os.Bundle
import com.appetiser.aldwin.R
import com.appetiser.aldwin.util.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
