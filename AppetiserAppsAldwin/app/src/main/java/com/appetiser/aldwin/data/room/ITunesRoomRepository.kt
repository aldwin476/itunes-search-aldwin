package com.appetiser.aldwin.data.room

import android.app.Application
import com.appetiser.aldwin.domain.entity.ResultEntity


interface ITunesRoomRepository {
    fun insert(result: ResultEntity?)
    fun getAllSavedData(): List<ResultEntity>
    fun getById(id: Int): ResultEntity
    fun deleteById(id: Int)
}

class ITunesRoomRepositoryImpl(application: Application) : ITunesRoomRepository {
    private val database =
        ResultsRoomDatabase.getDatabase(
            application
        )
    private val resultsDao = database.resultsDao()

    override fun insert(result: ResultEntity?) {
        resultsDao.insert(result)
    }

    override fun getAllSavedData(): List<ResultEntity> {
        return resultsDao.getAllSavedData()
    }

    override fun getById(id: Int): ResultEntity {
        return resultsDao.getByTrackId(id)
    }

    override fun deleteById(id: Int) {
        resultsDao.deleteById(id)
    }
}