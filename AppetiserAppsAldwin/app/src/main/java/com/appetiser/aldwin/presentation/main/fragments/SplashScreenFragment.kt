package com.appetiser.aldwin.presentation.main.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.appetiser.aldwin.R
import com.appetiser.aldwin.data.local.Preferences.lastVisitedDate
import com.appetiser.aldwin.data.local.Preferences.lastVisitedPage
import com.appetiser.aldwin.data.local.Preferences.navSource
import com.pawegio.kandroid.runDelayed
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SplashScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_splash, container, false)

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val formattedDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMMM dd,yyyy hh:mm a"))
        lastVisitedDate = formattedDate
        runDelayed(3000) {
            //Checks where the page should redirect on load
            when(lastVisitedPage){
                "details" -> {
                    navSource = "favorites"
                    findNavController().navigate(R.id.splash_to_details)
                }
                else -> findNavController().navigate(R.id.splash_to_main)
            }

        }
    }
}