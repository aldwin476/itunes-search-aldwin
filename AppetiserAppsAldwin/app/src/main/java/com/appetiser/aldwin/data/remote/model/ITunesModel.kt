package com.appetiser.aldwin.data.remote.model

data class ITunesModel(
    val resultsCount: Int,
    val results: List<ResultsModel>)
