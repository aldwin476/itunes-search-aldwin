package com.appetiser.aldwin.util.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.appetiser.aldwin.R

open class BaseFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}