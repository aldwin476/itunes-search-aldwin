package com.appetiser.aldwin.util.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.utilities.doNothing

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       doNothing()



    }
}
