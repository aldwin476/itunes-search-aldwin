package com.appetiser.aldwin.domain

import com.appetiser.aldwin.data.remote.ITunesService
import com.appetiser.aldwin.data.remote.model.ResultsModel
import io.reactivex.Completable

interface ITunesRepository {
    fun search(term: String?): Completable
    var results: MutableList<ResultsModel>
}

class ITunesRepositoryImpl(
    private val ITunesService: ITunesService
) : ITunesRepository {

    override var results: MutableList<ResultsModel> = mutableListOf()
    /**
     * Search function
     *
     * @param term Any [String] text to be used on search
     * @return items from api
     */
    override fun search(term: String?): Completable {
        return ITunesService.getItems(term)
            .map { response ->
                val responseBody = response.body()
                results = mutableListOf()
                responseBody?.results!!.forEach { result -> results.add(result) }

            }
            .ignoreElement()
    }


}
