package com.appetiser.aldwin.util

import android.content.Context
import com.appetiser.aldwin.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_itunes.view.*
import java.io.Serializable

class ITunesItems(
    private var artWorkSrc: String,
    private var trackName: String,
    private var genre: String,
    private var price: Double,
    private var con: Context
) : Item(), Serializable {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.apply {
            Glide.with(con)
                .load(artWorkSrc)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.placeholder_img)
                        .centerCrop()
                )
                .into(itemView.iv_artwork)
            itemView.tv_track_name.text = trackName
            itemView.tv_genre.text = genre
            itemView.tv_price.text = price.toString()
        }
    }

    override fun getLayout(): Int = R.layout.item_itunes
}