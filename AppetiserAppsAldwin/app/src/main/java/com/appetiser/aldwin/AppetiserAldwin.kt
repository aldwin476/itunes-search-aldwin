package com.appetiser.aldwin

import android.app.Application
import com.appetiser.aldwin._di.appModules
import com.chibatching.kotpref.Kotpref
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppetiserAldwin : Application() {

    override fun onCreate() {
        super.onCreate()

        // Kotpref
        Kotpref.init(this)
        // Koin
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@AppetiserAldwin)
            modules(appModules)

        }

    }

}
