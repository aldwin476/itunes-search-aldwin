package com.appetiser.aldwin.presentation.main.viewmodels

import com.appetiser.aldwin.data.room.ITunesRoomRepository
import com.appetiser.aldwin.domain.entity.ResultEntity
import com.appetiser.utilities.coroutines.RxViewModel
import com.appetiser.utilities.coroutines.SingleLiveEvent


class DetailsViewModel(
    private val iTunesRoomRepository: ITunesRoomRepository
) : RxViewModel() {
    private val _event = SingleLiveEvent<DetailsEvent>()
    val event: SingleLiveEvent<DetailsEvent> get() = _event

    /**
     * Get items by ID
     *
     * @param trackId Any [Int] number that will be used to search for specific items on the database
     */
    fun loadData(trackId: Int) {
        _event.value =
            DetailsEvent.OnLoaded(
                iTunesRoomRepository.getById(trackId)
            )
    }

    /**
     * Marks the item as favorites and will be saved on the database
     *
     * @param result Any [ResultEntity] entity that wil be used as indicator on which item to save as favorite
     */
    fun onFavoriteClicked(result: ResultEntity) {
        iTunesRoomRepository.insert(result)
        _event.value =
            DetailsEvent.OnFavoriteClicked

    }

    /**
     * Removes item from the database and favorites
     *
     * @param result Any [ResultEntity] entity that wil be used as indicator on which item to save as favorite
     */
    fun onRemoveFavoriteClicked(result: ResultEntity) {
        iTunesRoomRepository.deleteById(result.trackId)
        _event.value =
            DetailsEvent.OnRemoveFavoriteClicked

    }
}

//Events for handling actions on the viewmodel
sealed class DetailsEvent {
    object OnFavoriteClicked : DetailsEvent()
    object OnRemoveFavoriteClicked : DetailsEvent()
    data class  OnLoaded (val result: ResultEntity?) : DetailsEvent()
}