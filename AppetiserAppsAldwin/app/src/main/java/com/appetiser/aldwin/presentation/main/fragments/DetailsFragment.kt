package com.appetiser.aldwin.presentation.main.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.appetiser.aldwin.R
import com.appetiser.aldwin.data.local.Preferences.detailsId
import com.appetiser.aldwin.data.local.Preferences.lastVisitedPage
import com.appetiser.aldwin.data.remote.model.ResultsModel
import com.appetiser.aldwin.domain.entity.ResultEntity
import com.appetiser.aldwin.presentation.main.viewmodels.DetailsEvent
import com.appetiser.aldwin.presentation.main.viewmodels.DetailsViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.include_navigation_menu.*
import org.koin.androidx.viewmodel.ext.viewModel

class DetailsFragment : Fragment() {
    private val viewModel: DetailsViewModel by viewModel()
    private var isFavorite = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Observer for events on viewmodel
        viewModel.event.observe(this, Observer { event ->
            when (event) {
                is DetailsEvent.OnFavoriteClicked -> onFavoriteClicked()
                is DetailsEvent.OnRemoveFavoriteClicked -> oRemoveFavoriteClicked()
                is DetailsEvent.OnLoaded -> onLoaded(event.result)
            }
        })

        var resultEntity: ResultEntity? = null

        //Checker whether the page will load data from api or from database
        when (lastVisitedPage) {
            "search" -> {
                var result: ResultsModel = arguments?.get("result") as ResultsModel
                resultEntity = ResultEntity(
                    result.trackId,
                    result.trackName,
                    result.trackId,
                    result.trackPrice,
                    result.primaryGenreName,
                    result.artworkUrl100,
                    result.artistName,
                    result.releaseDate,
                    result.longDescription,
                    result.shortDescription
                )
            }
            "favorites" -> {
                resultEntity = arguments?.get("result") as ResultEntity
            }
            "details" -> {
                viewModel.loadData(detailsId)
            }
        }
        nav_back_btn.setOnClickListener {
            findNavController().navigate(R.id.details_to_main)
        }
        lastVisitedPage = "details"
        if (resultEntity != null) {
            nav_title.text = resultEntity.trackName
            viewModel.loadData(resultEntity.trackId)
            detailsId = resultEntity.trackId
            populateData(resultEntity, context!!)
            favorite_ic.setOnClickListener {
                if (isFavorite) viewModel.onRemoveFavoriteClicked(resultEntity)
                else viewModel.onFavoriteClicked(resultEntity)
            }
        }
    }

    /**
     * Populates the UI
     *
     * @param resultEntity Any [ResultEntity] entity that receives the actual data to be populated on the UI
     * @param context Any [Context] receives the context that will be used on glide
     */
    private fun populateData(resultEntity: ResultEntity, context: Context) {
        detailsId = resultEntity.trackId
        Glide.with(context)
            .load(resultEntity.artworkSrc)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.placeholder_img)
                    .centerCrop()
            )
            .into(details_artwork)
        details_track_name.text = resultEntity.trackName
        details_genre.text = resultEntity.genre
        details_artist.text = resultEntity.artistName
        details_release_date.text = resultEntity.releaseDate
        details_long_description.text = resultEntity.longDescription
    }


    //Contains actions when the favorite button was clicked
    private fun onFavoriteClicked() {
        favorite_ic.setImageResource(R.drawable.ic_star_on)
    }

    //Contains actions when you removed an item from
    private fun oRemoveFavoriteClicked() {
        favorite_ic.setImageResource(R.drawable.ic_star_off)
    }

    //Receiver when successfully loaded items from source
    private fun onLoaded(result: ResultEntity?) {
        if (result != null) {
            isFavorite = true
            populateData(result, context!!)
            favorite_ic.setImageResource(R.drawable.ic_star_on)
        }
    }


}