package com.appetiser.aldwin.presentation.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.aldwin.R
import com.appetiser.aldwin.data.local.Preferences.lastVisitedPage
import com.appetiser.aldwin.data.local.Preferences.navSource
import com.appetiser.aldwin.domain.entity.ResultEntity
import com.appetiser.aldwin.presentation.main.viewmodels.SavedItemEvent
import com.appetiser.aldwin.presentation.main.viewmodels.SavedItemsViewModel
import com.appetiser.aldwin.util.ITunesItems
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_saved_data.*
import kotlinx.android.synthetic.main.include_navigation_menu.*
import org.koin.androidx.viewmodel.ext.viewModel

class SavedDataFragment : Fragment() {
    private val viewModel: SavedItemsViewModel by viewModel()
    private var groupAdapter = GroupAdapter<GroupieViewHolder>()
    private var resultItems: List<ResultEntity> = mutableListOf()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.event.observe(this, Observer { event ->
            when (event) {
                is SavedItemEvent.OnGetSavedData -> onGetSavedData(event.results)
            }
        })
        lastVisitedPage = "favorites"
        nav_back_btn.visibility = View.GONE
        nav_title.text = "Favorites"
        rv_saved_items.apply {
            //Groupie Adapter to be used on the recycler
            groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
                setOnItemClickListener { item, view ->
                    navSource = "favorites"
                    lastVisitedPage = "favorites"
                    var bundle = bundleOf("result" to resultItems[getAdapterPosition(item)])
                    view.findNavController().navigate(R.id.main_to_details, bundle)
                }
            }
            layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = groupAdapter
        }
        viewModel.getSavedData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_saved_data, container, false)


    //Contains actions when successfully loaded data from database
    private fun onGetSavedData(results: List<ResultEntity>) {
        resultItems = results
        var iTunesItems: MutableList<ITunesItems> = mutableListOf()
        results.forEach { item ->
            iTunesItems.add(
                ITunesItems(
                    item.artworkSrc,
                    item.trackName,
                    item.genre,
                    item.price,
                    context!!
                )
            )
        }
        groupAdapter.clear()
        groupAdapter.addAll(iTunesItems)


    }
}