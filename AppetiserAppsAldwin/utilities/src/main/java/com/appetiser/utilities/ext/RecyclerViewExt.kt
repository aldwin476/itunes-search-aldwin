package com.appetiser.utilities.ext

import androidx.recyclerview.widget.RecyclerView

/**
 * Setup [RecyclerView]
 * @param layoutManager [RecyclerView.LayoutManager]
 * @param adapter [RecyclerView.Adapter]
 */
fun RecyclerView.setup(
    layoutManager: RecyclerView.LayoutManager,
    adapter: RecyclerView.Adapter<*>?) {
    this.layoutManager = layoutManager
    this.adapter = adapter
}
