package com.appetiser.utilities.ext

import android.widget.TextView

/**
 * Sanitized text
 */
fun TextView.text() = text.toString()

