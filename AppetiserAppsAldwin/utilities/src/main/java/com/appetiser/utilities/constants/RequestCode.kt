package com.appetiser.utilities.constants

object RequestCode {
	const val GALLERY_PICK = 59946
	const val TAKE_PHOTO = 58065
}