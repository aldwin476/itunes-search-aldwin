package com.appetiser.utilities.ext

import android.os.Bundle
import androidx.fragment.app.Fragment
import java.io.Serializable

fun Fragment.withArguments(vararg arguments: Pair<String, Serializable>): Fragment {
	val bundle = Bundle()
	arguments.forEach { bundle.putSerializable(it.first, it.second) }
	this.arguments = bundle
	return this
}

/**
 * Get argument from bundle
 * @param key key of argument
 */
fun <T : Any> Fragment.argument(key: String) =
	lazy { arguments?.get(key) as? T ?: error("Intent Argument $key is missing") }

/**
 * Get argument from bundle
 * @param key key of argument
 * @param defaultValue default value for argument
 */
fun <T : Any?> Fragment.argument(key: String, defaultValue: T? = null) =
	lazy { arguments?.get(key) as? T ?: defaultValue }
